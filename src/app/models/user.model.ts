export interface User {
  id: string;
  name: string;
  donated: number;
  contact?: string;
}
