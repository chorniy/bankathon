export interface Account {
    id: string;
    accountno: {
        number: string;
        bankCode: string;
        'cz-iban': string;
    };
    balance: {
        value: number;
    };
    productI18N: string;
}
