export interface Customer {
    firstName: string;
    lastName: string;
    idCard: {
        number: string;
    };
}
