export interface TransparentAccount {
    accountNumber: string;
    bankCode: string;
    actualizationDate: Date;
    balance: number;
    name: string;
    iban: string;
}

export interface TransparentAccountTransaction {
    processingDate: Date;
    typeDescription: string;
    amount: {
        value: number;
        currency: string;
    };
    sender: {
        accountNumber: string;
        bankCode: string;
        name: string;
        description: string
    };
    receiver: {
        accountNumber: string;
        bankCode: string;
    };
}
