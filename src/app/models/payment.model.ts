export interface Payment {
    paymentTypeInformation: {
        instructionPriority: string;
    };
    amount: {
        instructedAmount: {
            value: number;
            currency: string;
        };
    };
    requestedExecutionDate: Date;
    debtorAccount: {
        identification: {
            iban: string;
        }
    };
    creditorAccount: {
        identification: {
            iban: string;
        }
    };
}

export interface PaymentResponse {
    signInfo: SignInfo;
}

export interface SignInfo {
    signId: string;
    code: string;
}
