export interface Foundation {
  id: string;
  name: string;
  description: string;
  bankAccountNumber: string;
  bankCode: string;
  iban: string;
  logoSrc: string;
}
