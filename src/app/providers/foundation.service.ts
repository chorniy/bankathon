import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Foundation } from '../models/foundation.model';
import { FOUNDATIONS } from '../data/foundations.data';

@Injectable({
  providedIn: 'root',
})
export class FoundationService {

  /**
   * Get all the foundations
   */
  getFoundations(): Observable<Foundation[]> {
    return of(FOUNDATIONS);
  }

  /**
   * Get a foundation by id
   * @param id
   */
  getFoundationById(id: Foundation['id']): Observable<Foundation> {
    const theFoundation = FOUNDATIONS.find(foundation => foundation.id === id);
    return of(theFoundation);
  }
}
