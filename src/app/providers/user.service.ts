import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { User } from '../models/user.model';
import { from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: User;

  constructor() {
  }

  setUsers(users: User[]) {
    return from(Storage.set({
      key: 'users',
      value: JSON.stringify(users),
    }));
  }

  getUsers() {
    return from(Storage.get({ key: 'users' })).pipe(
      map(ret => {
        if (!ret) {
          return undefined;
        }
        const users: User[] = JSON.parse(ret.value);
        if (users && users.length) {
          return users;
        } else {
          return [];
        }
      })
    );
  }

  getUserById(id: User['id']) {
    return this.getUsers().pipe(
      map(users => users.find(user => user.id === id))
    );
  }

  increseUserDonation(amount: number) {
    const users = this.getUsers();
    return users.pipe(
      switchMap(val => {
        const user = val.find(u => u.id === this.currentUser.id);
        user.donated += amount;
        this.currentUser.donated += amount;

        return this.setUsers(val);
      })
    );
  }
}
