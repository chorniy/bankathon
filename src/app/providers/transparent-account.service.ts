import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransparentAccount, TransparentAccountTransaction } from '../models/transparent-account.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransparentAccountService {
  private url = `${environment.api}/v3/transparentAccounts`;

  constructor(private http: HttpClient) { }

  getTransparentAccount(id: string) {
    return this.http.get<TransparentAccount>(`${this.url}/${id}`);
  }

  getTransparentAccountTransaction(id: string): Observable<TransparentAccountTransaction[]> {
    return this.http.get<any>(`${this.url}/${id}/transactions`)
      .pipe(map(resp => resp.transactions));
  }
}
