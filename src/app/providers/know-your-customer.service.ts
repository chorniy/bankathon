import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Customer } from '../models/customer.model';

@Injectable({
  providedIn: 'root'
})
export class KnowYourCustomerService {
  private url = `${environment.api}/v2/kyc/my/profile`;

  constructor(private http: HttpClient) { }

  getCustomer() {
    return this.http.get<Customer>(this.url);
  }
}
