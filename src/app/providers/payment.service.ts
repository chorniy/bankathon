import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Payment, SignInfo, PaymentResponse } from '../models/payment.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private url = `${environment.api}/v1/payment-initiation/my/payments`;

  constructor(private http: HttpClient) { }

  createPayment(payment: Payment) {
    return this.http.post<PaymentResponse>(this.url, payment);
  }

  signPayment(signId: string, authizationType: string) {
    return this.http.post<PaymentResponse>(`${this.url}/sign/${signId}`, { authizationType });
  }

  confirmPayment(signId: string, authizationType: string, oneTimePassword: string) {
    return this.http.put<SignInfo>(`${this.url}/sign/${signId}`, { authizationType, oneTimePassword });
  }
}
