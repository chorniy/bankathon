import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FoundationDetailPage } from './foundation-detail.page';
import { PaymentModalComponent } from './payment-modal/payment-modal.component';

const routes: Routes = [
  {
    path: '',
    component: FoundationDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [PaymentModalComponent],
  declarations: [FoundationDetailPage, PaymentModalComponent]
})
export class FoundationDetailPageModule {}
