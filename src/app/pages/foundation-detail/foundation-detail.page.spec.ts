import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundationDetailPage } from './foundation-detail.page';

describe('FoundationDetailPage', () => {
  let component: FoundationDetailPage;
  let fixture: ComponentFixture<FoundationDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundationDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundationDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
