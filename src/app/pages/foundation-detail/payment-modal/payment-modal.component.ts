import { Component, OnInit, Input } from '@angular/core';
import { Account } from 'src/app/models/account.model';
import { AccountService } from 'src/app/providers/account.service';
import { Observable } from 'rxjs';
import { Payment } from 'src/app/models/payment.model';
import { PaymentService } from 'src/app/providers/payment.service';
import { switchMap } from 'rxjs/operators';
import { ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/providers/user.service';

@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.scss'],
})
export class PaymentModalComponent implements OnInit {
  @Input() iban: string;

  accounts$: Observable<Account[]>;
  selectedAccount: Account;

  step = 0;

  amount: number;
  code: string;

  signId: string;

  constructor(
      private accountService: AccountService,
      private paymentService: PaymentService,
      private userService: UserService,
      private modalCtrl: ModalController,
      private toastCtrl: ToastController
  ) {
  }

  ngOnInit() {
    this.accounts$ = this.accountService.getAccounts();
  }

  pay() {
    const payment: Payment = {
      amount: {
        instructedAmount: {
          value: this.amount,
          currency: 'CZK'
        }
      },
      creditorAccount: {
        identification: {
          iban: this.selectedAccount.accountno['cz-iban']
        }
      },
      debtorAccount: {
        identification: {
          iban: this.iban
        }
      },
      paymentTypeInformation: {
        instructionPriority: 'HIGH'
      },
      requestedExecutionDate: new Date()
    };

    this.paymentService.createPayment(payment).pipe(
        switchMap(resp => this.paymentService.signPayment(resp.signInfo.signId, 'TAC'))
    ).subscribe(
        resp => {
          this.signId = resp.signInfo.signId;
          this.step = 1;
        }
    );
  }

  confirm() {
    this.paymentService.confirmPayment(this.signId, 'TAC', this.code).pipe(
        switchMap(_ => this.userService.increseUserDonation(this.amount))
    ).subscribe(
        _ => {
          const toast = this.toastCtrl.create({
            message: 'Yeah! You have increased your karma!',
            duration: 5000,
            color: 'success',
          });

          toast.then(t => t.present());

          this.modalCtrl.dismiss();
        }
    );
  }

  close() {
    this.modalCtrl.dismiss();
  }
}
