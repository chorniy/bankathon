import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { Foundation } from '../../models/foundation.model';
import { FoundationService } from '../../providers/foundation.service';
import { TransparentAccount, TransparentAccountTransaction } from 'src/app/models/transparent-account.model';
import { TransparentAccountService } from 'src/app/providers/transparent-account.service';
import { ModalController, ToastController } from '@ionic/angular';
import { PaymentModalComponent } from './payment-modal/payment-modal.component';
import { UserService } from '../../providers/user.service';

@Component({
  selector: 'app-foundation-detail',
  templateUrl: './foundation-detail.page.html',
  styleUrls: ['./foundation-detail.page.scss'],
})
export class FoundationDetailPage implements OnInit {
  id$: Observable<string> = this.activatedRoute.paramMap.pipe(map(params => params.get('id')));

  foundation$: Observable<Foundation> = this.id$.pipe(
    switchMap(id => this.foundationService.getFoundationById(id)),
    );
  foundation: Foundation;

  accountDetail$: Observable<TransparentAccount> = this.foundation$.pipe(
    switchMap(foundation =>
      this.transparentAccountService.getTransparentAccount(foundation.bankAccountNumber))
  );

  transactions$: Observable<TransparentAccountTransaction[]> = this.foundation$.pipe(
    switchMap(foundation =>
      this.transparentAccountService.getTransparentAccountTransaction(foundation.bankAccountNumber)),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private foundationService: FoundationService,
    private transparentAccountService: TransparentAccountService,
    public modalController: ModalController,
    private userService: UserService,
    private toastCtrl: ToastController,
  ) {
  }

  ngOnInit() {
    this.foundation$.subscribe(
      resp => this.foundation = resp
    );
  }

  subscribe() {
    const toast = this.toastCtrl.create({
      message: 'Coming soon...',
      duration: 3000,
      color: 'medium',
    });

    toast.then(t => t.present());
  }

  async openPaymentModal() {
    if (this.userService.currentUser) {
      const modal = await this.modalController.create({
        component: PaymentModalComponent,
        componentProps: {
          iban: this.foundation.iban
        }
      });
      modal.present();
    } else {
      this.router.navigate(['/user-info']);
    }
  }
}
