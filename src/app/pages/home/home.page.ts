import { Component } from '@angular/core';
import { UserService } from '../../providers/user.service';
import { User } from '../../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  users: User[] = [];

  constructor(
      private userService: UserService,
      private router: Router,
  ) {
    this.userService.getUsers().subscribe(users => {
      this.users = users.sort((a, b) => {
        return b.donated - a.donated;
      });
      console.log(this.users);
    });
  }

  donate() {
    if (this.userService.currentUser) {
      this.router.navigate(['/foundation-list']);
    } else {
      this.router.navigate(['/user-info']);
    }
  }
}
