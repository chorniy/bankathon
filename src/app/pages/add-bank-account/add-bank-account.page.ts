import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-bank-account',
  templateUrl: './add-bank-account.page.html',
  styleUrls: ['./add-bank-account.page.scss'],
})
export class AddBankAccountPage implements OnInit {
  form: FormGroup = new FormGroup({
    accountNumber: new FormControl('', [Validators.required, Validators.maxLength(16)]),
    bankCode: new FormControl('', [Validators.required, Validators.maxLength(4)]),
  });

  constructor() {
  }

  ngOnInit() {
  }

  save() {
    console.log(this.form);
  }
}
