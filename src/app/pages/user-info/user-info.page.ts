import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';
import { Account } from 'src/app/models/account.model';
import { UserService } from 'src/app/providers/user.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.page.html',
  styleUrls: ['./user-info.page.scss'],
})
export class UserInfoPage implements OnInit {
  accounts$: Observable<Account>;

  constructor(
    private oauthService: OAuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  login() {
    this.oauthService.initLoginFlow();
  }

  get accessToken() {
    return this.oauthService.getAccessToken();
  }

  get user() {
    return this.userService.currentUser;
  }
}
