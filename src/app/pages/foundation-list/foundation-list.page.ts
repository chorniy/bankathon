import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Foundation } from '../../models/foundation.model';
import { FoundationService } from '../../providers/foundation.service';

@Component({
  selector: 'app-foundation-list',
  templateUrl: './foundation-list.page.html',
  styleUrls: ['./foundation-list.page.scss'],
})
export class FoundationListPage implements OnInit {
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<{ title: string; note: string; icon: string }> = [];
  foundations$: Observable<Foundation[]> = this.foundationService.getFoundations();

  constructor(
      private foundationService: FoundationService,
  ) {
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  ngOnInit() {
  }

}
