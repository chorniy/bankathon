import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundationListPage } from './foundation-list.page';

describe('FoundationListPage', () => {
  let component: FoundationListPage;
  let fixture: ComponentFixture<FoundationListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundationListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
