import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';

import { authConfig } from './config/auth.config';
import { UserService } from './providers/user.service';
import { KnowYourCustomerService } from './providers/know-your-customer.service';
import { switchMap } from 'rxjs/operators';
import { USERS } from './data/users.data';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Foundation list',
      url: '/foundation-list',
      icon: 'list'
    },
    {
      title: 'My account',
      url: '/user-info',
      icon: 'person'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oauthService: OAuthService,
    private userService: UserService,
    private knowYourCustomerService: KnowYourCustomerService
  ) {
    this.initializeApp();

    this.authConfigure();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.userService.getUsers().subscribe(users => {
        if (!users || !users.length) {
          this.userService.setUsers(USERS).subscribe();
        }
      });

      if (this.oauthService.hasValidAccessToken()) {
        this.getUser();
      }
    });
  }

  private authConfigure() {
    this.oauthService.configure(authConfig);
    this.oauthService.customQueryParams = {
      access_type: 'offline'
    };
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.tryLogin();

    this.oauthService.events.subscribe(
      event => {
        if (event.type === 'token_received') {
          this.getUser();
        }
      }
    );
    this.oauthService.setupAutomaticSilentRefresh();
  }

  private getUser() {
    this.knowYourCustomerService.getCustomer().pipe(
      switchMap(resp => this.userService.getUserById(resp.idCard.number))
    ).subscribe(
      user => this.userService.currentUser = user
    );
  }
}
