import { Injectable, Optional } from '@angular/core';
import { OAuthStorage, OAuthResourceServerErrorHandler, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private authStorage: OAuthStorage,
        private errorHandler: OAuthResourceServerErrorHandler,
        @Optional() private moduleConfig: OAuthModuleConfig
    ) {
    }

    private checkUrl(url: string): boolean {
        const found = this.moduleConfig.resourceServer.allowedUrls.find(u => url.startsWith(u));
        return !!found;
    }

    private tokenUrl(url: string): boolean {
        const found = url.includes('token');
        return found;
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const url = req.url.toLowerCase();

        // TODO: NOT FOR PRODUCTION. WE DO NOT USE BACKEND!!!
        const sendSecretKey = this.tokenUrl(url);
        if (sendSecretKey) {
            const secretKey = environment.secretKey;

            const body = req.body.set('client_secret', secretKey);
            req = req.clone({ body });
        }

        if (!this.moduleConfig) { return next.handle(req); }
        if (!this.moduleConfig.resourceServer) { return next.handle(req); }
        if (!this.moduleConfig.resourceServer.allowedUrls) { return next.handle(req); }
        if (!this.checkUrl(url)) { return next.handle(req); }

        const apiKey = environment.apiKey;
        let headers = req.headers
            .set('web-api-key', apiKey);

        const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;
        if (sendAccessToken) {

            const token = this.authStorage.getItem('access_token');
            const header = 'Bearer ' + token;

            headers = headers
                .set('Authorization', header);
        }

        req = req.clone({ headers });

        return next.handle(req).pipe(
            catchError(err => this.errorHandler.handleError(err))
        );
    }
}
