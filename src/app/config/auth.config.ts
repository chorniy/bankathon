import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  //   Url of the Identity Provider
  loginUrl: 'https://webapi.developers.erstegroup.com/api/csas/sandbox/v1/sandbox-idp/auth',

  tokenEndpoint: 'https://webapi.developers.erstegroup.com/api/csas/sandbox/v1/sandbox-idp/token',

  //   issuer: 'https://webapi.developers.erstegroup.com/api/csas/sandbox/v1/sandbox-idp/',
  //   strictDiscoveryDocumentValidation: false,

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin + '/login-success',

  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: 'c2ad9879-dca8-42ac-b70a-fab542bd4964',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'PISP peract coract kyc AISP',

  responseType: 'code',

  oidc: false
};
