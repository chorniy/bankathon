import { Foundation } from '../models/foundation.model';

export const FOUNDATIONS: Foundation[] = [
  {
    id: '1',
    name: 'First one',
    description: 'We are the first charity foundation in history.',
    bankAccountNumber: '000000-0109213309',
    bankCode: '0800',
    iban: 'CZ75 0800 0000 0001 0921 3309',
    logoSrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBZfExLU7ATGGggUgCOpflbze6krYP-FgEY-ecULYLQXdlVdy9',
  },
  {
    id: '2',
    name: 'For all',
    description: 'We help everybody.',
    bankAccountNumber: '000000-3396076389',
    bankCode: '0800',
    iban: 'CZ63 0800 0000 0004 6004 3319',
    logoSrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBZfExLU7ATGGggUgCOpflbze6krYP-FgEY-ecULYLQXdlVdy9',
  },
  {
    id: '3',
    name: 'The Charitiest!',
    description: 'We are The Charitiest!',
    bankAccountNumber: '000000-3936173359',
    bankCode: '0800',
    iban: 'CZ19 0800 0000 0039 3617 3359',
    logoSrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBZfExLU7ATGGggUgCOpflbze6krYP-FgEY-ecULYLQXdlVdy9',
  },
];
