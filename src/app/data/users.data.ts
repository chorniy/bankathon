import { User } from '../models/user.model';

export const USERS: User[] = [
  {
    id: '1',
    name: 'Alex',
    donated: 1445,
    contact: 'Hey guys, check it out my instagram account @alex :)',
  },
  {
    id: '123456789',
    name: 'Max',
    donated: 0,
    contact: 'Photographer portfolio -> http://max.com',
  },
  {
    id: '3',
    name: 'Dan',
    donated: 4500,
    contact: 'I am talking about money investments http://youtube.com/channels/dan',
  },
  {
    id: '4',
    name: 'Jiri Novotny',
    donated: 150,
    contact: '',
  },
  {
    id: '5',
    name: 'Kuba',
    donated: 1500,
    contact: '',
  },
  {
    id: '6',
    name: 'Jaroslav',
    donated: 1500,
    contact: '',
  },
  {
    id: '7',
    name: 'Unknown cat',
    donated: 350,
    contact: 'I am kitty cat',
  },
  {
    id: '8',
    name: 'Unknown user',
    donated: 690,
    contact: '',
  },
];
