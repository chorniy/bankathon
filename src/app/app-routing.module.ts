import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'foundation-list',
    loadChildren: () => import('./pages/foundation-list/foundation-list.module').then(m => m.FoundationListPageModule),
  },
  {
    path: 'foundation-detail/:id',
    loadChildren: () => import('./pages/foundation-detail/foundation-detail.module').then(m => m.FoundationDetailPageModule),
  },
  {
    path: 'add-bank-account',
    loadChildren: () => import('./pages/add-bank-account/add-bank-account.module').then(m => m.AddBankAccountPageModule),
  },
  {
    path: 'login-success',
    redirectTo: 'home',
    pathMatch: 'full',
    // loadChildren: () => import('./pages/login-success/login-success.module').then(m => m.LoginSuccessPageModule),
  },
  {
    path: 'user-info',
    loadChildren: () => import('./pages/user-info/user-info.module').then(m => m.UserInfoPageModule),
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
